<?php
namespace T3easy\Basecontent\Hooks;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Jan Kiesewetter <jan@t3easy.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
class PageLayoutViewDrawItemHook implements \TYPO3\CMS\Backend\View\PageLayoutViewDrawItemHookInterface {

	/**
	 * string backPath
	 */
	private $backPath = '';

	/**
	 * Create thumbnail code for record/field
	 *
	 * @param array $row Record array
	 * @param string $table Table (record is from)
	 * @param string $field Field name for which thumbnails are to be rendered.
	 * @return string HTML for thumbnails, if any.
	 * @todo Define visibility
	 */
	public function thumbCode($row, $table, $field) {
		return \TYPO3\CMS\Backend\Utility\BackendUtility::thumbCode(
			$row,
			$table,
			$field,
			$this->backPath,
			$thumbScript = '',
			$uploaddir = NULL,
			$abs = 0,
			$tparams = '',
			$size = '',
			$linkInfoPopup = TRUE
		);
	}

	/**
	 * Preprocesses the preview rendering of a content element.
	 *
	 * @param \TYPO3\CMS\Backend\View\PageLayoutView $parentObject Calling parent object
	 * @param boolean $drawItem Whether to draw the item using the default functionalities
	 * @param string $headerContent Header content
	 * @param string $itemContent Item content
	 * @param array $row Record row of tt_content
	 * @return void
	 */
	public function preProcess(\TYPO3\CMS\Backend\View\PageLayoutView &$parentObject, &$drawItem, &$headerContent, &$itemContent, array &$row) {
		switch ($row['CType']) {
			case 'basecontent_slider':
				$drawItem = FALSE;
				if ($row['image']) {
					$itemContent = $this->thumbCode($row, 'tt_content', 'image');
				}
				break;
		}
	}
}
?>